# primeros 2 bytes :
# 00 = login      , 
# 01 = posicion
# 02 = evento
#
########### caso login:
# respuesta : 100
# 
########### caso posicion:
# respuesta: si es valido=111, si es invalido=110
# dato | bytes
# id = 2-9
# timestamp = 10-19
# sur o norte = 20 (SUR vuelve negativo la coordenada)
# lat = 21-27
# oeste o este = 28 (OESTE vuelve negativo la coordenada)
# lon = 29-35
# validez = 36 (A=valido,V=invalido)
# velocidad = 37-38
# 
#
#
########### caso evento
#respusta : 120
# dato | bytes
# output_code = 2-3
# output_value = 4-5
require 'active_support/all'
class Decoder

	@id
	@datetime
	@speed
	@valid
	@lat
	@lon
	@response
	@pack

	attr_accessor :id, :datetime, :speed, :valid, :lat, :lon, :response

	def initialize

	end

	def read(pack)
		@pack = pack
		self
	end

	def login
		if @pack == "00"
			@response = "100"
			return true
		end  
		return false
	end
end